<?php

use Doctrine\DBAL\Driver\PDOMySql\Driver;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => Driver::class,
                'params' => [
                    'host'     => 'localhost',                    
                    'user'     => 'game',
                    'dbname'   => 'game',
                    'password' => 'game'
                ]
            ],            
        ],        
    ],
    'navigation' => [
        'default' => [
            [
                'label' => 'Home',
                'route' => 'home'
            ],
            [
                'label' => 'Sign up',
                'route' => 'default',
                'controller' => 'register',
                'action'     => 'index'
            ]
        ],
        'login' => [
            [
                'label' => 'Home',
                'route' => 'home'
            ],
            [
                'label' => 'Log out',
                'route' => 'logout'
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'navigation' => Zend\Navigation\Service\NavigationAbstractServiceFactory::class,
        ]
    ]
];
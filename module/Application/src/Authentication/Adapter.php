<?php

namespace Application\Authentication;

use Application\Entity\User;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class Adapter implements AdapterInterface
{
    protected $user;
    protected $password;

    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    public function authenticate()
    {
        if ($this->user->authenticate($this->password)) {
            if ($this->user->isActivated()) {
                return new Result(Result::SUCCESS, $this->user);
            } else {
                return new Result(Result::FAILURE, $this->user);
            }
        } else {
            if (is_null($this->user->id)) {
                return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->user);
            }
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, $this->user);
        }

        return new Result(Result::FAILURE_UNCATEGORIZED, $this->user);
    }
}
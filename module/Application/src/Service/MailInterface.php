<?php

namespace Application\Service;

use Sendgrid\Email;

interface MailInterface
{
    public function getClient();
    public function init();
    public function send($mail);
}
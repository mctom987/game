<?php

namespace Application\Service;

use Application\Service\MailInterface;
use SendGrid\Client;
use SendGrid\Mail;

class MailService implements MailInterface
{
    const VERSION = '1.0.0';

    public $version = self::VERSION;

    protected $apiKey;
    protected $host = 'https://api.sendgrid.com';
    protected $apiVersion = '/v3';
    protected $client;

    public function __construct(Client $client = null)
    {
        if ($client) {
            $this->client = $client;
        }
    }

    public function send($mail)
    {
        if (Mail::class == get_class($mail)) {
            return $this->client->mail()->send()->post($mail);
        } else {
            // Unknown mail type… try sending anyway
            return $this->client->mail()->send()->post($mail);
        }
    }

    public function init($curlOptions = null)
    {
        $headers = [
            'Authorization: Bearer ' . $this->apiKey,
            'User-Agent: potato-game/' . $this->version . ';php',
            'Accept: application/json'
        ];
        $this->client = new Client($this->host, $headers, $this->apiVersion, null, $curlOptions);

        return $this;
    }

    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setApiKey($key)
    {
        $this->apiKey = $key;
        return $this;
    }

    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    public function setApiVersion($version)
    {
        $this->apiVersion = $version;
        return $this;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getApiVersion()
    {
        return $this->apiVersion;
    }
}
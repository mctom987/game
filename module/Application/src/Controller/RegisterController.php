<?php

namespace Application\Controller;

use Application\Entity\User;
use SendGrid;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

/**
 * Handles registration of new users
 * @todo Remove advertisement of existing emails, and instead email the owner "account exists, forgot your password?"
 */
class RegisterController extends ApplicationAbstractActionController
{
    public function indexAction()
    {
        $view = $this->getView();
        $form = $this->getForm();
        $form->prepare();
        $view->form = $form;

        return $view;
    }

    public function submitAction()
    {
        $view = $this->getView();
        $form = $this->getForm();
        $form->setData($this->request->getPost());

        $verifyInput = new Input('verify');
        $passwordValidator = new Validator\Identical($form->get('password')->getValue());
        $verifyInput->getValidatorChain()->attach($passwordValidator);

        $filter = $form->getInputFilter();
        $filter->add($verifyInput);
        $form->setInputFilter($filter);

        if ($form->isValid()) {
            // Yay!
            $validatedData = $form->getData();

            $user = new User();
            $user->email    = $validatedData['email'];
            $user->password = $validatedData['password'];

            $em = $this->getEntityManager();

            // Check for existing user
            $existing = $em->getRepository(User::class)->findOneByEmail($user->email);
            if ($existing) {
                // User already exists
                $form->get('email')->setMessages(['A user with this email already exists']);
                $form->get('password')->setValue('');
                $form->get('verify')->setValue('');
                $view->form = $form;
                $view->setTemplate('application/register/index.phtml');
            } else {
                $user->generateActivationCode();
                // Save user
                $em->persist($user);
                $em->flush();

                $mi = $this->getMailInterface()->init();

                $confirmationLink = $this->url()->fromRoute('activate', ['code' => $user->activationCode], ['force_canonical' => TRUE]);

                $from = new SendGrid\Email('Potato Game', 'potato@crummett.us');
                $to   = new SendGrid\Email($user->email, $user->email);
                $subject = 'Signup Confirmation';
                $body = new SendGrid\Content('text/plain',
                        "This email is to confirm you recently signed up for our services.\n"
                        . "If you did not sign up, no worries. Nothing is needed from you.\n\n"
                        . "In order to finish the registration process, please confirm your email\n"
                        . "address by navigating to the following URL.\n\n"
                        . $confirmationLink . "\n\n"
                        . "Thanks, from all of us on the team.\n\n"
                        . 'Terms of Service: ' . $this->url()->fromRoute('tos', [], ['force_canonical' => TRUE])
                );

                $mail = new SendGrid\Mail($from, $subject, $to, $body);

                $response = $mi->send($mail);

                if ($response->statusCode() == 202) {
                    // Success
                } else {
                    // Email failed to send
                    $this->flashMessenger()
                        ->addMessage('There was an error during registration. Please contact support for assistance.');
                }
            }
        } else {
            // Sadface
            $view->errors = $form->getMessages();

            $form->get('password')->setValue('');
            $form->get('verify')->setValue('');
            $view->form = $form;
            $view->setTemplate('application/register/index.phtml');
        }

        return $view;
    }

    public function activateAction()
    {
        $code = $this->params('code');
        $em = $this->getEntityManager();
        $repo = $em->getRepository(User::class);
        $user = $repo->findOneByActivationCode($code);

        if (!$user) {
            // Cannot find user from code, possibly already activated
            $this->flashMessenger()
                ->addMessage('Invalid activation code. Is your account already activated?');
            return $this->redirect()->toRoute('home');
        } else {
            $user->activate();
            $em->persist($user);
            $em->flush();

            $this->flashMessenger()
                ->addMessage('Account activated. You may now log in.');
        }

        return $this->forward()->dispatch('index', ['action'=>'index']);
    }

    protected function getForm()
    {
        $form = new Form();

        $form->setData($this->request->getPost());

        $email = new Element\Email('email');
        $email->setLabel('Email address')
            ->setAttribute('required', true);

        $password = new Element\Password('password');
        $password->setLabel('Password')
            ->setAttribute('required', true);

        $verify = new Element\Password('verify');
        $verify->setLabel('Verify password')
            ->setAttribute('required', true);

        $tos = new Element\Checkbox('tos');
        $tos->setLabel('Accept Terms of Service')
            ->setAttribute('required', true);

        $submit = new Element\Submit('submit');
        $submit->setValue('Submit');

        $form->add($email)
            ->add($password)
            ->add($verify)
            ->add($tos)
            ->add($submit);

        $form->setAttribute('action', $this->url()->fromRoute('default', array('controller' => 'register', 'action'=> 'submit')))
            ->setAttribute('method', 'post');

        return $form;

    }
}
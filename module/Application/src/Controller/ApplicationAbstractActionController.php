<?php

namespace Application\Controller;

use Application\Service\MailService;
use Doctrine\ORM\EntityManager;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;


class ApplicationAbstractActionController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $view;

    protected $authService;

    protected $mailInterface;

    protected $config;

    const SESSION_NAME = 'session';

    public function onDispatch(MvcEvent $e)
    {
        $this->setNav();

        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->entityManager = $em;
        return $this;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function setMailInterface(MailService $mi)
    {
        $this->mailInterface = $mi;
        return $this;
    }

    public function getMailInterface()
    {
        return $this->mailInterface;
    }

    /**
     * Returns the view model, and instantiates a new one if not yet created
     */
    public function getView()
    {
        if (is_null($this->view)) {
            $this->view = $this->acceptableViewModelSelector();
        }
        return $this->view;
    }

    /**
     * Manages the session
     */
    public function getAuthService()
    {
        if (is_null($this->authService)) {
            $session = new Session(self::SESSION_NAME);
            $this->authService = new AuthenticationService($session);
        }
        return $this->authService;
    }

    public function setNav()
    {
        if ($this->getAuthService()->hasIdentity()) {
            $this->layout()->navName = 'Zend\Navigation\Login';
        } else {
            $this->layout()->navName = 'Zend\Navigation\Default';
        }
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }
}
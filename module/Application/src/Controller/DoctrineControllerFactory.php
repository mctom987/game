<?php

namespace Application\Controller;

use Application\Service\MailInterface;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class DoctrineControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new $requestedName();

        $config = $container->get('config');

        $em = $container->get(EntityManager::class);
        $controller->setEntityManager($em);


        $mi = $container->get(MailInterface::class);
        $mi->setApiKey($config['sendgrid']['api_key']);

        $controller->setMailInterface($mi);

        return $controller;
    }
}
<?php

namespace Application\Controller;

use Application\Entity\User;
use Application\Authentication\Adapter as AuthenticationAdapter;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

class IndexController extends ApplicationAbstractActionController
{
    protected $form;

    public function indexAction()
    {
        $view = $this->getView();
        if ($this->getAuthService()->hasIdentity()) {
            // Welcome back!
            return $this->forward()->dispatch('index', ['action'=>'logged-in']);
        } else {
            $view->form = $this->form ? $this->form : $this->getLoginForm();
        }

        return $view;
    }

    public function loginAction()
    {
        $form = $this->getLoginForm();

        $form->setData($this->request->getPost());
        $this->form = $form;

        if ($form->isValid()) {
            // Form data is valid, check login
            $validatedData = $form->getData();
            $em = $this->getEntityManager();
            $repo = $em->getRepository(User::class);

            $email    = $validatedData['email'];
            $password = $validatedData['password'];

            $user = $repo->findOneByEmail($email);

            if (is_null($user)) {
                // No user found, login failed
                // Set a dummy user to emulate password check against a real user
                // This helps mitigate checking existance from an unauthenticated client
                $user = new User();
            }

            $authService = $this->getAuthService();
            $authenticator = new AuthenticationAdapter($user, $password);
            $authResult = $authService->authenticate($authenticator);

            if ($authResult->isValid()) {
                // Login success
                return $this->redirect()->toRoute('home');
            } else {
                // Login failed
                $this->form->get('email')->setMessages(['Login failed']);

                return $this->forward()->dispatch('index', ['action'=>'index']);
            }
        } else {
            return $this->forward()->dispatch('index', ['action'=>'index']);
        }
    }

    public function logoutAction()
    {
        $this->getAuthService()->clearIdentity();
        $this->flashMessenger()->addMessage('You have been logged out.');

        return $this->redirect()->toRoute('home');
    }

    public function tosAction()
    {
        return $this->getView();
    }

    public function loggedInAction()
    {
        return $this->getView();
    }

    public function getLoginForm()
    {
        $form = new Form();

        $email = new Element\Email('email');
        $email->setLabel('Email address')
            ->setAttribute('required', true);

        $password = new Element\Password('password');
        $password->setLabel('Password')
            ->setAttribute('required', true);

        $submit = new Element\Submit('submit');
        $submit->setValue('Submit');

        $form->setAttribute('action', $this->url()->fromRoute('login'))
            ->setAttribute('method', 'post');

        $form->add($email)
            ->add($password)
            ->add($submit);

        return $form;
    }
}
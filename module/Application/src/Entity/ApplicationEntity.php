<?php

namespace Application\Entity;

use Application\Entity\Exception\ImmutablePropertyException;
use Application\Entity\Exception\UnknownPropertyException;
use Doctrine\ORM\Mapping as ORM;
use Zend\Crypt\Key\Derivation\Scrypt;
use Zend\Math\Rand;

abstract class ApplicationEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")   
     */
    protected $id;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        if (!$this->id) {
            $this->id = $id;
        } else {
            // Immutable
            throw new ImmutablePropertyException('id');
        }
    }

    public function __get($field)
    {
        $getter = 'get' . ucfirst($field);
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } else {
            throw new UnknownPropertyException($field);
        }
    }

    public function __set($field, $value)
    {

        $setter = 'set' . ucfirst($field);
        if (method_exists($this, $setter)) {
            return $this->$setter($value);
        } else {
            throw new UnknownPropertyException($field);
        }
    }
}
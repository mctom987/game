<?php

namespace Application\Entity\Exception;

use Exception;

/**
 * Thrown when an immutable property is attempted to be changed
 */
class ImmutablePropertyException extends Exception
{
    public function __construct($property)
    {
        parent::__construct('Immutable property "' . $property . '"');
    }
}
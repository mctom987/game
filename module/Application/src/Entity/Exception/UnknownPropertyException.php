<?php

namespace Application\Entity\Exception;

use Exception;

/**
 * Thrown when an property is accessed which does not exist
 */
class UnknownPropertyException extends Exception
{
    public function __construct($property)
    {
        parent::__construct('Unknown property "' . $property . '"');
    }
}
<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Router\Segment;
use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'index',
                        'action'     => 'index',
                    ],
                ],
            ],
            'default' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/[:controller[/:action]]',
                    'defaults' => [
                        'controller' => 'index',
                        'action'     => 'index',
                    ],
                    'skippable' => [
                        'controller' => true,
                        'action'     => true
                    ]
                ],
            ],
            'activate' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/activate/:code',
                    'defaults' => [
                        'controller' => 'register',
                        'action'     => 'activate',
                    ]
                ],
            ],
            'login' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => 'index',
                        'action'     => 'login'
                    ]
                ]
            ],
            'logout' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => 'index',
                        'action'     => 'logout'
                    ]
                ]
            ],
            'tos' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/terms-of-service',
                    'defaults' => [
                        'controller' => 'index',
                        'action'     => 'tos'
                    ]
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Controller\DoctrineControllerFactory::class,
            Controller\RegisterController::class => Controller\DoctrineControllerFactory::class,
        ],
        'aliases' => [
            'index' => Controller\IndexController::class,
            'register' => Controller\RegisterController::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    'service_manager' => [
        'invokables' => [
            'Application\Service\MailInterface' => 'Application\Service\MailService'
        ]
    ]
];